﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Models
{
    public interface IVersioned
    {
        long Id { get; }
        long Version { get; }
    }
}
