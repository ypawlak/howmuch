﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Models
{
    public class Expense : IVersioned
    {
        public Expense()
        {
            this.Shares = new HashSet<Share>();
            this.Payments = new HashSet<Payment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long AccountingId { get; set; }

        public String Name { get; set; }
        public decimal Value { get; set; }

        public virtual ICollection<Share> Shares { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }

        public long Version { get; set; }
    }
}
