﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Models
{
    public class AccountingUser
    {
        public long AccountingId { get; set; }
        public long UserId { get; set; }

        public Accounting Accounting { get; set; }
        public User User { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime JoinTimestamp { get; set; }
    }
}
