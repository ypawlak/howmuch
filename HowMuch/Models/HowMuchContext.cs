﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace HowMuch.Models
{
    public class HowMuchContext : DbContext
    {

        public HowMuchContext(DbContextOptions<HowMuchContext> options)
            : base(options)
        {
        }

        public DbSet<Accounting> Accountings { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AccountingUser> AccountingsUsers { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<Share> Shares { get; set; }
        public DbSet<Payment> Payments { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountingUser>()
                .HasKey(au => new { au.UserId, au.AccountingId });

            modelBuilder.Entity<AccountingUser>()
                .HasOne(au => au.Accounting)
                .WithMany(a => a.AccountingsUsers)
                .HasForeignKey(au => au.AccountingId);

            modelBuilder.Entity<AccountingUser>()
                .HasOne(au => au.User)
                .WithMany(u => u.AccountingsUsers)
                .HasForeignKey(au => au.UserId);
        }
    }
}
