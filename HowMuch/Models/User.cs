﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Models
{
    public class User : IVersioned
    {
        public User()
        {
            this.AccountingsUsers = new HashSet<AccountingUser>();
            this.Shares = new HashSet<Share>();
            this.Payments = new HashSet<Payment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AccountingUser> AccountingsUsers { get; set; }
        public virtual ICollection<Share> Shares { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public long Version { get; set; }
    }
}
