﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Models
{
    public class Accounting : IVersioned
    {
        public Accounting()
        {
            this.Expenses = new HashSet<Expense>();
            this.AccountingsUsers = new HashSet<AccountingUser>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Expense> Expenses { get; set; }
        public virtual ICollection<AccountingUser> AccountingsUsers { get; set; }
        public long Version { get; set; }
    }
}
