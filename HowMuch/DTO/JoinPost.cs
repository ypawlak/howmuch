﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class JoinPost
    {
        public long JoinUserId { get; set; }
        public long AccountingId { get; set; }
    }
}
