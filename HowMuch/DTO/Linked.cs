﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public abstract class Linked
    {
        protected abstract string ResourceUrlLabel { get; }
        public long Id { get; set; }
        public IDictionary<NavigationKey, string> Links { get; protected set; }
        
        public Linked(long id)
        {
            this.Id = id;
            this.Links = new Dictionary<NavigationKey, string>();
        }

        protected void AppendCommonLinks(string urlPrefix)
        {
            Links.Add(NavigationKey.SELF, $"{urlPrefix}/{ResourceUrlLabel}/{Id}");
        }
        
    }
}
