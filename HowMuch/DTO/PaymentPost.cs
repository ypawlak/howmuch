﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class PaymentPost
    {
        public long UserId { get; set; }
        public decimal Value { get; set; }
    }
}
