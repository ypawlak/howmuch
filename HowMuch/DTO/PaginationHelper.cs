﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public static class PaginationHelper
    {
        public static IList<T> Page <T> (List<T> toPage, PaginationParams pagination)
        {
            if (pagination != null && (pagination.Start ?? 0) >= 0)
            {
                int pagStart = pagination.Start ?? 0;
                Console.WriteLine($"pagParams params: start[{pagination.Start}] limit {pagination.Limit}");
                if (pagStart + pagination.Limit < toPage.Count)
                    return toPage.ToList().GetRange(pagStart, pagination.Limit);
                else if(pagStart < toPage.Count)
                    return toPage.ToList().GetRange(pagStart, toPage.Count - pagStart);
            }

            return toPage;
        }

        public static void AppendPageLinks <T> (List<T> toPage, PaginationParams pagination,
            IDictionary<NavigationKey, string> toAppend, string url)
        {
            if (pagination == null)
                return;

            int pagStart = pagination.Start ?? 0;

            if (pagStart + pagination.Limit < toPage.Count)
            {
                toAppend.Add(NavigationKey.NEXT,
                    $"{url}?start={pagStart + pagination.Limit}&limit={pagination.Limit}");
            }

            if(pagStart != 0)
            {
                int prevStart = pagStart - pagination.Limit;
                if (prevStart < 0)
                    prevStart = 0;

                toAppend.Add(NavigationKey.PREV,
                    $"{url}?start={prevStart}&limit={pagination.Limit}");
            }
        }
    }
}
