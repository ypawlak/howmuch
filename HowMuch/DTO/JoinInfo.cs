﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class JoinInfo : Linked
    {
        public JoinInfo(AccountingUser dbModel, string baseUrl) : base(dbModel.Id)
        {
            AppendCommonLinks(baseUrl);
            Timestamp = dbModel.JoinTimestamp;
            Accounting = new LightAccountingInfo(dbModel.Accounting, baseUrl);
            User = new LightUserInfo(dbModel.User, baseUrl);
        }

        public DateTime Timestamp { get; private set; }
        public LightAccountingInfo Accounting { get; private set; }
        public LightUserInfo User { get; private set; }

        protected override string ResourceUrlLabel => "Joins";
    }
}
