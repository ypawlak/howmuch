﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class LightExpenseInfo : VersionedLinked
    {
        public LightExpenseInfo(Expense dbModel, string urlPrefix)
            : base(dbModel)
        {
            this.Name = dbModel.Name;
            this.Value = dbModel.Value;
            AppendCommonLinks(urlPrefix);
        }

        public string Name { get; set; }
        public decimal Value { get; set; }

        protected override string ResourceUrlLabel => "Expenses";
    }
}
