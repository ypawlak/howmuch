﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class LightAccountingInfo : Linked
    {
        public LightAccountingInfo(Accounting dbModel, string urlPrefix)
            : base(dbModel.Id)
        {
            this.Name = dbModel.Name;
            AppendCommonLinks(urlPrefix);
        }

        public string Name { get; private set; }

        protected override string ResourceUrlLabel => "Accountings";
    }
}
