﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public abstract class VersionedLinked : Linked
    {
        public long Version { get; private set; }
        public VersionedLinked(IVersioned dbModel) : base(dbModel.Id)
        {
            Version = dbModel.Version;
        }
    }
}
