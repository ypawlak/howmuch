﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public enum NavigationKey
    {
        BASE,
        NEXT,
        PREV,
        SELF
    }
}
