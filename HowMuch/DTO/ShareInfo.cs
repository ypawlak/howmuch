﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class ShareInfo
    {
        public ShareInfo(Share dbModel, string baseUrl)
        {
            this.User = new UserIdLinked(dbModel.UserId, baseUrl);
            this.Percentage = dbModel.Percentage;
        }
        
        public UserIdLinked User { get; private set; }
        public decimal Percentage { get; private set; }
    }
}
