﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class LightUserInfo : UserVersionedInfo
    {
        public LightUserInfo(User dbModel, string urlPrefix) : base(dbModel)
        {
            this.Name = dbModel.Name;
            AppendCommonLinks(urlPrefix);
        }

        public string Name { get; private set; }
    }
}
