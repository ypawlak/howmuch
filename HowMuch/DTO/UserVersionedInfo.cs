﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public abstract class UserVersionedInfo : VersionedLinked
    {
        public UserVersionedInfo(User dbModel) : base(dbModel)
        {
        }

        protected override string ResourceUrlLabel => "Users";
    }
}
