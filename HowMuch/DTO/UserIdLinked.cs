﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class UserIdLinked : Linked
    {
        public UserIdLinked(long id, string prefixUrl) : base(id)
        {
            AppendCommonLinks(prefixUrl);
        }

        protected override string ResourceUrlLabel => "Users";
    }
}
