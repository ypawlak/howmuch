﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class UserDashboardInfo : LightUserInfo
    {
        public UserDashboardInfo(User user, string prefixUrl)
            : base(user, prefixUrl)
        {
            this.Accountings = user.AccountingsUsers.Select(au
                => new LightAccountingInfo(au.Accounting, Links[NavigationKey.SELF]))
                .ToList();
            //this.Credits = new List<long>();
            //this.Debits = new List<long>();
        }
        public ICollection<LightAccountingInfo> Accountings { get; set; }
        //public ICollection<long> Debits { get; set; }
        //public ICollection<long> Credits { get; set; }
        
    }
}
