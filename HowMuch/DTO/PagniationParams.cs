﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class PaginationParams
    {
        public int Limit { get; set; }
        public int? Start { get; set; }
    }
}
