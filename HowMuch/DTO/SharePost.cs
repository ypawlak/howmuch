﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class SharePost
    {
        public long UserId { get; set; }
        public decimal Percentage { get; set; }
    }
}
