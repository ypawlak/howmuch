﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class UserEditable
    {
        public string Name { get; set; }
        public long? LastVersion { get; set; }
    }
}
