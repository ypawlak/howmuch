﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class AccountingEditable
    {
        public string Name { get; set; }
        public long? LastVersion { get; set; }
        public ICollection<long> Users { get; set; }
    }
}
