﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class ExpensePost
    {
        public String Name { get; set; }
        public decimal Value { get; set; }
        public virtual IEnumerable<SharePost> Shares { get; set; }
        public virtual IEnumerable<PaymentPost> Payments { get; set; }

        public long? LastVersion { get; set; }
    }
}
