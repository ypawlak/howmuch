﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class FullAccountingInfo : VersionedLinked
    {

        public FullAccountingInfo(Accounting dbModel, string urlPrefix, PaginationParams pagParams = null)
            : base(dbModel)
        {
            this.Name = dbModel.Name;
            AppendCommonLinks(urlPrefix);

            List<LightExpenseInfo> toPage = dbModel.Expenses
                    .Select(e =>
                        new LightExpenseInfo(e, Links[NavigationKey.SELF]))
                    .ToList();
            this.Expenses = PaginationHelper.Page(toPage, pagParams);

            PaginationHelper.AppendPageLinks(toPage, pagParams, Links, Links[NavigationKey.SELF]);
        }

        public string Name { get; private set; }

        public ICollection<LightExpenseInfo> Expenses { get; private set; }


        protected override string ResourceUrlLabel => "Accountings";

    }
}
