﻿using HowMuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.DTO
{
    public class PaymentInfo
    {
        public PaymentInfo(Payment dbModel, string baseUrl)
        {
            this.User = new UserIdLinked(dbModel.UserId, baseUrl);
            this.Value = dbModel.Value;
        }

        public UserIdLinked User { get; private set; }
        public decimal Value { get; private set; }
    }
}
