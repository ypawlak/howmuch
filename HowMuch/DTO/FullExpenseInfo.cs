﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HowMuch.Models;

namespace HowMuch.DTO
{
    public class FullExpenseInfo : LightExpenseInfo
    {
        public FullExpenseInfo(Expense dbModel, string urlPrefix, string baseUrl) : base(dbModel, urlPrefix)
        {
            Shares = dbModel.Shares.Select(sh => new ShareInfo(sh, baseUrl));
            Payments = dbModel.Payments.Select(p => new PaymentInfo(p, baseUrl));
        }

        public IEnumerable<ShareInfo> Shares { get; private set; }
        public IEnumerable<PaymentInfo> Payments { get; private set; }
    }
}
