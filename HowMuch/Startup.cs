﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HowMuch.Models;

namespace HowMuch
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<HowMuchContext>(opt => opt.UseInMemoryDatabase("HowMuchContext"));
            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var context = app.ApplicationServices.GetService<HowMuchContext>();
            //AddTestData(context);

            app.UseMvc();
        }

        private static void AddTestData(HowMuchContext context)
        {
            User usr = new User();
            usr.Name = "Andrzej";
            context.Users.Add(usr);

            Accounting acc = new Accounting()
            {
                Name = "Wyjazd"
            };
            context.Accountings.Add(acc);
            context.SaveChanges();
            AccountingUser au = new AccountingUser();
            au.AccountingId = acc.Id;
            au.UserId = usr.Id;
            context.AccountingsUsers.Add(au);
            context.SaveChanges();

            Expense exp = new Expense()
            {
                Name = "Zakupy",
                Value = 100
            };

            exp.AccountingId = acc.Id;
            context.Expenses.Add(exp);
            context.SaveChanges();

            Share shr = new Share()
            {
                Percentage = 100
            };
            shr.ExpenseId = exp.Id;
            shr.UserId = usr.Id;
            context.Shares.Add(shr);
            context.SaveChanges();

            Payment paym = new Payment()
            {
                Value = 100
            };
            paym.ExpenseId = exp.Id;
            paym.UserId = usr.Id;

            context.Payments.Add(paym);
            context.SaveChanges();
        }
    }
}
