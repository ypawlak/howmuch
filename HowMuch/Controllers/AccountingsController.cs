using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HowMuch.Models;
using HowMuch.DTO;
using Microsoft.Extensions.Primitives;

namespace HowMuch.Controllers
{
    [Produces("application/json")]
    [Route("api/Accountings")]
    [Route("api/Users/{userId:long?}/Accountings")]
    public class AccountingsController : Controller
    {
        private readonly HowMuchContext _context;
        public AccountingsController(HowMuchContext context)
        {
            _context = context;
        }

        // GET: api/Accountings
        [HttpGet]
        public IEnumerable<LightAccountingInfo> GetAccountings()
        {
            var result = _context.Accountings.Include(a => a.AccountingsUsers)
                .Include("Expenses.Shares")
                .Include("AccountingsUsers.User").ToList();

            return result.Select(a =>
                new LightAccountingInfo(a, RequestHelper.GetAncestorPath(Request.Path.Value)));
        }

        [HttpGet]
        [Route("{accountingId:long}")] // Matches GET api/users/123/accountings/456
        public async Task<IActionResult> GetAccounting([FromRoute]long? userId, [FromRoute]long accountingId,
            [FromQuery(Name = "start")] string start, [FromQuery(Name = "limit")] string limit)
        {
            PaginationParams pagParams = RequestHelper.GetPaginationParamsFromRequest(start, limit);
            if (userId.HasValue)
            {
                var user = _context.Users
                    .Include(u => u.AccountingsUsers)
                    .FirstOrDefault(u => u.Id == userId);
                if (user != null && user.AccountingsUsers.Any(au => au.AccountingId == accountingId))
                    return await LoadAccounting(accountingId, pagParams);
            }
            else
            {
                return await LoadAccounting(accountingId, pagParams);
            }

            return BadRequest(ModelState);
        }

        private async Task<IActionResult> LoadAccounting(long id, PaginationParams pagParams)
        {
            if (!ModelState.IsValid)
            {
                Console.WriteLine("invalid model state");
                return BadRequest(ModelState);
            }

            var accounting = await _context.Accountings
                .Include(a => a.Expenses)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (accounting == null)
                return NotFound();

            string ancestorPath = RequestHelper.GetAncestorPath(Request.Path);
            var result = new FullAccountingInfo(accounting, ancestorPath, pagParams);

            //TODO: possibly inherit from controller which handles POE
            if (Request.Headers.ContainsKey(PoeLinksHandler.PoeRequestHeader))
            {
                string expensePostUrl = $"{Request.Path}/Expenses";
                Response.Headers.Add(PoeLinksHandler.ExpensesPoeHandler.GetNewPoeLinkHeader(expensePostUrl));
            }

            return Ok(result);
        }

        // PUT: api/Accountings/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccounting([FromRoute] long id, [FromBody] AccountingEditable accPut)
        {
            if (!ModelState.IsValid || accPut == null || !accPut.LastVersion.HasValue
                || string.IsNullOrEmpty(accPut.Name) || accPut.Users == null || !accPut.Users.Any())
            {
                return BadRequest(ModelState);
            }

            if (!AccountingExists(id))
                return NotFound();
            
            Accounting editedAccounting =_context.Accountings
                .Include(a => a.AccountingsUsers)
                .First(e => e.Id == id);

            if(editedAccounting.Version != accPut.LastVersion.Value)
                return new StatusCodeResult(StatusCodes.Status409Conflict);

            IList<AccountingUser> droppedUsers = editedAccounting.AccountingsUsers
                .Where(au => !accPut.Users.Contains(au.UserId)).ToList();
            IList<long> joinedUsers = accPut.Users
                .Where(u =>
                    !editedAccounting.AccountingsUsers.Any(au => au.UserId == u))
                .ToList();

            IList<User> newUsers = _context.Users.Where(u => joinedUsers.Contains(u.Id)).ToList();
            if (newUsers.Count < joinedUsers.Count)
                return BadRequest(ModelState);

            editedAccounting.Name = accPut.Name;
            editedAccounting.Version++;
            Console.WriteLine($"PUT new name {accPut.Name}");
            _context.Entry(editedAccounting).State = EntityState.Modified;

            foreach (long userId in joinedUsers)
            {
                var joinDbModel = new AccountingUser();
                joinDbModel.AccountingId = editedAccounting.Id;
                joinDbModel.UserId = userId;
                joinDbModel.JoinTimestamp = DateTime.Now;
                _context.AccountingsUsers.Add(joinDbModel);
                await _context.SaveChangesAsync();
            }

            foreach (AccountingUser join in droppedUsers)
            {
                _context.AccountingsUsers.Remove(join);
                await _context.SaveChangesAsync();
            }
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountingExists(id))
                    return NotFound();
                else
                    throw;
            }

            return NoContent();
        }

        // POST: api/Accountings
        [HttpPost]
        public async Task<IActionResult> PostAccounting([FromBody] AccountingEditable accPost)
        {
            if (!ModelState.IsValid || accPost == null ||
                string.IsNullOrEmpty(accPost.Name) || accPost.Users == null || !accPost.Users.Any())
            {
                return BadRequest(ModelState);
            }

            IList<User> users = _context.Users.Where(u => accPost.Users.Contains(u.Id)).ToList();
            if(users.Count < accPost.Users.Count)
                return BadRequest(ModelState);

            var toAdd = new Accounting()
            {
                Name = accPost.Name,
                Version = 1
            };

            _context.Accountings.Add(toAdd);
            await _context.SaveChangesAsync();

            foreach (long userId in accPost.Users)
            {
                var joinDbModel = new AccountingUser();
                joinDbModel.AccountingId = toAdd.Id;
                joinDbModel.UserId = userId;
                joinDbModel.JoinTimestamp = DateTime.Now;
                _context.AccountingsUsers.Add(joinDbModel);
                await _context.SaveChangesAsync();
            }

            return Created("GetAccounting",
                new FullAccountingInfo(toAdd, RequestHelper.GetAncestorPath(Request.Path.Value)));
        }

        // DELETE: api/Accountings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccounting([FromRoute] long id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var accounting = await _context.Accountings
                .Include("Expenses.Shares")
                .Include("Expenses.Payments")
                .SingleOrDefaultAsync(m => m.Id == id);

            if (accounting == null)
            {
                return NotFound();
            }

            var joins = _context.AccountingsUsers
                        .Where(au => au.AccountingId == id).ToList();
            joins.ForEach(j => _context.AccountingsUsers.Remove(j));

            accounting.Expenses.ToList().ForEach(e => ExpensesController.DeleteWithShares(e, _context));
            _context.Accountings.Remove(accounting);
            await _context.SaveChangesAsync();

            return Ok(accounting);
        }

        private bool AccountingExists(long id)
        {
            return _context.Accountings.Any(e => e.Id == id);
        }
    }
}