﻿using HowMuch.DTO;
using HowMuch.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Controllers
{
    [Produces("application/json")]
    [Route("api/Joins/")]
    public class JoinController : Controller
    {
        private readonly HowMuchContext _context;

        public JoinController(HowMuchContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<JoinInfo> GetJoins()
        {
            var joins = _context.AccountingsUsers
                .Include(j => j.Accounting)
                .Include(j => j.User)
                .ToList();

            return joins.Select(au =>
                new JoinInfo(au, RequestHelper.GetAncestorPath(Request.Path)));
        }

        // GET: api/Expenses/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetJoin([FromRoute] long id)
        {
            var join = await _context.AccountingsUsers
                .Include(j => j.Accounting)
                .Include(j => j.User)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (join == null)
                return NotFound();

            var response = new JoinInfo(join, RequestHelper.GetAncestorPath(Request.Path.Value));
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> PostJoin([FromBody] JoinPost join)
        {
            if (!ModelState.IsValid || join == null)
            {
                return BadRequest(ModelState);
            }

            Accounting accounting = await _context.Accountings
                .SingleOrDefaultAsync(a => a.Id == join.AccountingId);

            User user = await _context.Users
                .SingleOrDefaultAsync(u => u.Id == join.JoinUserId);

            if(accounting == null || user == null)
                return BadRequest(ModelState);

            AccountingUser existing = await _context.AccountingsUsers
                .SingleOrDefaultAsync(au => au.AccountingId == join.AccountingId && au.UserId == join.JoinUserId);
            if(existing != null)
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            
            var joinDbModel = new AccountingUser();
            joinDbModel.AccountingId = accounting.Id;
            joinDbModel.UserId = user.Id;
            joinDbModel.JoinTimestamp = DateTime.Now;
            _context.AccountingsUsers.Add(joinDbModel);
            
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJoin", new { id = joinDbModel.Id },
                new JoinInfo(joinDbModel, RequestHelper.GetAncestorPath(Request.Path.Value)));
        }

        // DELETE: api/Accountings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJoin([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var join = await _context.AccountingsUsers.SingleOrDefaultAsync(m => m.Id == id);
            if (join == null)
            {
                return NotFound();
            }

            _context.AccountingsUsers.Remove(join);
            await _context.SaveChangesAsync();

            return Ok(join);
        }

        public static bool HasUserJoinedAccounting(long usrId, long accId, HowMuchContext ctx)
        {
            return ctx.AccountingsUsers
                .Any(au => au.AccountingId == accId && au.UserId == usrId);
        }
    }
}
