﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Controllers
{ 
    public class PoeLinksHandler
    {
        private static PoeLinksHandler _expensesPoeHandler = new PoeLinksHandler();
        public static PoeLinksHandler ExpensesPoeHandler => _expensesPoeHandler;

        public const string PoeRequestHeader = "POE";
        public const string PoeParameterName = "POE-Guid";
        private const string PoeLinksHeader = "POE-Links";
        private IDictionary<Guid, bool> _linkGuids;
        public PoeLinksHandler()
        {
            _linkGuids = new Dictionary<Guid, bool>();
        }

        public KeyValuePair<string, StringValues> GetNewPoeLinkHeader(string poeUrlBase)
        {
            Guid newGuid = Guid.NewGuid();
            _linkGuids.Add(newGuid, false);
            string poedUrl = poeUrlBase.Contains("?")
                ? $"{poeUrlBase}&{PoeParameterName}={newGuid}"
                : $"{poeUrlBase}?{PoeParameterName}={newGuid}";

            return new KeyValuePair<string, StringValues>(PoeLinksHeader, poedUrl);
        }
        public bool IsGenuine(Guid guid)
        {
            return _linkGuids.ContainsKey(guid);
        }
        public bool IsExploited(Guid guid)
        {
            return _linkGuids[guid];
        }

        public void MarkExploited(Guid guid)
        {
            _linkGuids[guid] = true;
        }

        public void UnmarkExploited(Guid guid)
        {
            _linkGuids[guid] = false;
        }
    }
}
