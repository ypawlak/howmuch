using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HowMuch.Models;
using HowMuch.DTO;
using System.Net;
using Microsoft.Net.Http.Headers;

namespace HowMuch.Controllers
{
    [Produces("application/json")]
    //[Route("api/Expenses")]
    [Route("api/Accountings/{accountingId:long?}/Expenses")]
    [Route("api/Users/{userId:long?}/Accountings/{accountingId:long}/Expenses")]
    public class ExpensesController : Controller
    {
        private readonly HowMuchContext _context;

        public ExpensesController(HowMuchContext context)
        {
            _context = context;
        }

        // GET: api/Expenses
        [HttpGet]
        public IActionResult GetExpenses([FromRoute]long? accountingId)
        {
            if (!accountingId.HasValue)
                return BadRequest(ModelState);

            IList<Expense> expenses = _context.Expenses.Where(e => e.AccountingId == accountingId.Value)
                .ToList();
            IEnumerable<LightExpenseInfo> result = expenses.Select(e => new LightExpenseInfo(e,
                RequestHelper.GetAncestorPath(Request.Path)));

            return Ok(result);
        }

        // GET: api/Expenses/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetExpense([FromRoute]long? accountingId, [FromRoute] long id)
        {
            if (!ModelState.IsValid || !accountingId.HasValue)
                return BadRequest(ModelState);

            var expense = await _context.Expenses
                .Include(e => e.Payments)
                .Include(e => e.Shares)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (expense == null)
                return NotFound();
            if (expense.AccountingId != accountingId.Value)
                return BadRequest(ModelState);

            var result = new FullExpenseInfo(expense, RequestHelper.GetAncestorPath(Request.Path),
                RequestHelper.GetBasePath(Request.Path));

            //var result = new ExpensePost()
            //{
            //    Name = expense.Name,
            //    Value = expense.Value,
            //    Shares = expense.Shares.Select(sh => new SharePost()
            //    {
            //       Percentage = sh.Percentage,
            //       UserId = sh.UserId
            //    }),
            //    Payments = expense.Payments.Select(paym => new PaymentPost()
            //    {
            //        UserId = paym.UserId,
            //        Value = paym.Value
            //    })

            //};
            return Ok(result);
        }

        // PUT: api/Expenses/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExpense([FromRoute] long id, [FromRoute]long? accountingId,
            [FromBody] ExpensePost expensePut)
        {
            if (!ModelState.IsValid || !accountingId.HasValue
                || expensePut == null || string.IsNullOrEmpty(expensePut.Name)
                || !expensePut.Shares.Any() || !expensePut.Payments.Any()
                || expensePut.Shares.Any(e => e.UserId == 0 || e.Percentage < 0 || e.Percentage > 100)
                || expensePut.Payments.Any(p => p.UserId == 0 || p.Value == 0)
                || expensePut.Shares.Sum(sh => sh.Percentage) != 100
                || expensePut.Value != expensePut.Payments.Sum(p => p.Value)
                || !expensePut.LastVersion.HasValue)
            {
                return BadRequest(ModelState);
            }

            var expense = await _context.Expenses
                .Include(e => e.Payments)
                .Include(e => e.Shares)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (expense == null)
                return NotFound();

            if (expense.Version != expensePut.LastVersion.Value)
                return new StatusCodeResult(StatusCodes.Status409Conflict);

            expense.Name = expensePut.Name;
            expense.Value = expensePut.Value;
            expense.Version++;

            var shares = new List<Share>();
            foreach (SharePost shPost in expensePut.Shares)
            {
                if (!JoinController.HasUserJoinedAccounting(shPost.UserId, accountingId.Value, _context))
                    return BadRequest(ModelState);

                shares.Add(new Share { Percentage = shPost.Percentage, UserId = shPost.UserId });
            }

            var payments = new List<Payment>();
            foreach (PaymentPost paymPost in expensePut.Payments)
            {
                if (!JoinController.HasUserJoinedAccounting(paymPost.UserId, accountingId.Value, _context))
                    return BadRequest(ModelState);

                payments.Add(new Payment { Value = paymPost.Value, UserId = paymPost.UserId });
            }
            
            expense.Shares.ToList().ForEach(sh => _context.Shares.Remove(sh));
            expense.Payments.ToList().ForEach(p => _context.Payments.Remove(p));
            _context.Entry(expense).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            shares.ForEach(sh => { sh.ExpenseId = expense.Id; _context.Shares.Add(sh); });
            payments.ForEach(p => { p.ExpenseId = expense.Id; _context.Payments.Add(p); });
            
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Expenses
        [HttpPost]
        public async Task<IActionResult> PostExpense([FromRoute]long? accountingId, [FromBody] ExpensePost expensePost,
            [FromQuery(Name = PoeLinksHandler.PoeParameterName)] string poeGuid)
        {
            if (!ModelState.IsValid || !accountingId.HasValue
                || expensePost == null || string.IsNullOrEmpty(expensePost.Name)
                || !expensePost.Shares.Any() || !expensePost.Payments.Any()
                || expensePost.Shares.Any(e => e.UserId == 0 || e.Percentage < 0 || e.Percentage > 100)
                || expensePost.Payments.Any(p => p.UserId == 0 || p.Value == 0)
                || expensePost.Shares.Sum(sh => sh.Percentage) != 100
                || expensePost.Value != expensePost.Payments.Sum(p => p.Value))
            {
                return BadRequest(ModelState);
            }

            //TODO: possibly inherit from controller which handles POE
            if (Request.Headers.ContainsKey(PoeLinksHandler.PoeRequestHeader))
            {
                if(string.IsNullOrEmpty(poeGuid) || !Guid.TryParse(poeGuid, out Guid parsedPoeGuid)
                    || !PoeLinksHandler.ExpensesPoeHandler.IsGenuine(parsedPoeGuid))
                {
                    return BadRequest(ModelState);
                }
                else if(PoeLinksHandler.ExpensesPoeHandler.IsExploited(parsedPoeGuid))
                {
                    var result = new ObjectResult(poeGuid)
                    {
                        StatusCode = (int)HttpStatusCode.MethodNotAllowed
                    };
                    Response.Headers.Add(HeaderNames.Allow, System.Net.Http.HttpMethod.Get.ToString());
                    return result;
                }

                PoeLinksHandler.ExpensesPoeHandler.MarkExploited(Guid.Parse(poeGuid));
            }

            Expense addedExpense = new Expense()
            {
                Name = expensePost.Name,
                Value = expensePost.Value,
                AccountingId = accountingId.Value,
                Version = 1
            };

            try
            {
                var shares = new List<Share>();
                foreach (SharePost shPost in expensePost.Shares)
                {
                    if (!JoinController.HasUserJoinedAccounting(shPost.UserId, accountingId.Value, _context))
                        return BadRequest(ModelState);
                    AccountingUser existing = await _context.AccountingsUsers
                        .SingleOrDefaultAsync(au => au.AccountingId == accountingId.Value && au.UserId == shPost.UserId);
                    if (existing == null)
                        return BadRequest(ModelState);

                    Console.WriteLine($"Expense POST base on join {existing.Id}");

                    shares.Add(new Share { Percentage = shPost.Percentage, UserId = shPost.UserId });
                }

                var payments = new List<Payment>();
                foreach (PaymentPost paymPost in expensePost.Payments)
                {
                    if (!JoinController.HasUserJoinedAccounting(paymPost.UserId, accountingId.Value, _context))
                        return BadRequest(ModelState);

                    AccountingUser existing = await _context.AccountingsUsers
                        .SingleOrDefaultAsync(au => au.AccountingId == accountingId.Value && au.UserId == paymPost.UserId);
                    if (existing == null)
                        return BadRequest(ModelState);

                    Console.WriteLine($"Expense POST base on join {existing.Id}");

                    payments.Add(new Payment { Value = paymPost.Value, UserId = paymPost.UserId });
                }

                _context.Expenses.Add(addedExpense);
                await _context.SaveChangesAsync();
                shares.ForEach(sh => { sh.ExpenseId = addedExpense.Id; _context.Shares.Add(sh); });
                payments.ForEach(p => { p.ExpenseId = addedExpense.Id; _context.Payments.Add(p); });

                await _context.SaveChangesAsync();
                //await Task.Delay(20000);
            }
            catch
            {
                //TODO: possibly inherit from controller which handles POE
                if (Request.Headers.ContainsKey(PoeLinksHandler.PoeRequestHeader))
                    PoeLinksHandler.ExpensesPoeHandler.UnmarkExploited(Guid.Parse(poeGuid));
                throw;
            }

            return CreatedAtAction("GetExpense", new { id = addedExpense.Id }, addedExpense);
        }

        // DELETE: api/Expenses/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExpense([FromRoute] long id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var expense = await _context.Expenses
                .Include(e => e.Payments)
                .Include(e => e.Shares)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (expense == null)
                return NotFound();

            expense.Shares.ToList().ForEach(sh => _context.Shares.Remove(sh));
            expense.Payments.ToList().ForEach(p => _context.Payments.Remove(p));
            _context.Expenses.Remove(expense);
            await _context.SaveChangesAsync();

            return Ok(expense);
        }

        public static void DeleteWithShares(Expense expense, HowMuchContext ctx)
        {
            expense.Shares.ToList().ForEach(sh => ctx.Shares.Remove(sh));
            expense.Payments.ToList().ForEach(p => ctx.Payments.Remove(p));
            ctx.Expenses.Remove(expense);
        }

        private bool ExpenseExists(long id)
        {
            return _context.Expenses.Any(e => e.Id == id);
        }
    }
}