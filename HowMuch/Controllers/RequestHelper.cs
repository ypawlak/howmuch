﻿using HowMuch.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HowMuch.Controllers
{
    public class RequestHelper
    {
        public static string GetBasePath(string requestPath)
        {
            if (requestPath.Contains("/"))
            {
                int secondSlashIndex = requestPath.IndexOf('/', requestPath.IndexOf('/') + 1);
                if(secondSlashIndex > 0)
                    requestPath = requestPath.Substring(0, secondSlashIndex);
            }
            return requestPath;
        }
        public static string GetAncestorPath(string requestPath)
        {
            return GetAncestorPath(requestPath, 1);
        }
        private static string GetAncestorPath(string requestPath, int depth = 1)
        {
            for (int i = 0; i < depth + 1; i++)
            {
                if (requestPath.Contains("/"))
                    requestPath = requestPath.Substring(0, requestPath.LastIndexOf("/"));
            }

            return requestPath;
        }

        public static PaginationParams GetPaginationParamsFromRequest(string startParam, string limitParam)
        {
            return string.IsNullOrEmpty(limitParam) || !int.TryParse(limitParam, out int parsedLimit)
                ? null
                : new PaginationParams()
                {
                    Limit = parsedLimit,
                    Start = int.TryParse(startParam, out int parsedStart)
                            ? (int?)parsedStart
                            : null
                };

        }
    }
}
