using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HowMuch.Models;
using HowMuch.DTO;

namespace HowMuch.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly HowMuchContext _context;

        public UsersController(HowMuchContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<UserDashboardInfo> GetUsers()
        {
            return _context.Users.Include(u => u.AccountingsUsers)
                .Include("AccountingsUsers.Accounting")
                .AsNoTracking()
                .Select(u
                    => new UserDashboardInfo(u, RequestHelper.GetAncestorPath(Request.Path.Value)));
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser([FromRoute] long id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _context.Users
                .Include(u => u.AccountingsUsers)
                .Include("AccountingsUsers.Accounting")
                .SingleOrDefaultAsync(m => m.Id == id);

            if (user == null)
                return NotFound();

            var response = new UserDashboardInfo(user, RequestHelper.GetAncestorPath(Request.Path.Value));

            return Ok(response);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] long id, [FromBody] UserEditable user)
        {
            if (!ModelState.IsValid || !user.LastVersion.HasValue)
                return BadRequest(ModelState);

            var dbUser = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

            if (user == null)
                return NotFound();
            if(user.LastVersion != dbUser.Version)
                return new StatusCodeResult(StatusCodes.Status409Conflict);

            dbUser.Name = user.Name;
            dbUser.Version++;
            _context.Entry(dbUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                    return NotFound();
                else
                    throw;
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] UserEditable user)
        {
            if (!ModelState.IsValid || user == null || string.IsNullOrEmpty(user.Name))
            {
                return BadRequest(ModelState);
            }

            var toAdd = new User() { Name = user.Name, Version = 1 };
            _context.Users.Add(toAdd);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser",
                new { id = toAdd.Id },
                new UserDashboardInfo(toAdd, RequestHelper.GetAncestorPath(Request.Path.Value)));
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] long id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
                return NotFound();

            if(_context.Shares.Any(sh => sh.UserId == id) || _context.Payments.Any(p => p.UserId == id))
                return BadRequest(ModelState);
            var joins =  _context.AccountingsUsers
                        .Where(au => au.UserId == id).ToList();
            joins.ForEach(j => _context.AccountingsUsers.Remove(j));
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        private bool UserExists(long id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        public static bool UserExists(long id, HowMuchContext ctx)
        {
            return ctx.Users.Any(e => e.Id == id);
        }
    }
}